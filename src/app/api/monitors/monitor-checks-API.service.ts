import { Injectable, inject } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { baseUrl } from "../../constants";

@Injectable({
  providedIn: "root",
})
export class MonitorChecksAPIService {
  private http = inject(HttpClient);

  list(
    organizationSlug: string,
    monitorId: string,
    cursor?: string | null,
    isChange = true
  ) {
    let httpParams = new HttpParams();
    const url = `${baseUrl}/organizations/${organizationSlug}/monitors/${monitorId}/checks/`;
    if (cursor) {
      httpParams = httpParams.set("cursor", cursor);
    }
    if (isChange) {
      httpParams = httpParams.set("is_change", "true");
    }
    return this.http.get(url, {
      observe: "response",
      params: httpParams,
    });
  }
}
